#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>

#define MAXINPUT 9

int main()
{
    char input[MAXINPUT] = "";
    int length,i;

    printf("Please enter a phone number:\n+36-");
    scanf ("%s", input);
    length = strlen (input);
    for (i=0;i<length; i++)
        if (!isdigit(input[i]))
        {
            printf ("Invalid phone number\n");
            exit(1);
        }
    printf ("Valid phone number\n");
}
